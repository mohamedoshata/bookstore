package bookstore.entities;

public enum ActionEnum {
    ViewAll(1,"View All books"),AddNew(2,"Add new book"),Edit(3,"Edit book"),Search(4,"search"),Save(5,"Save and exit");

    private String value;
    private int key;

    ActionEnum(int key,String value) {
        this.value = value;
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public int getKey() {
        return key;
    }
}

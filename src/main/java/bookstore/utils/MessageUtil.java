package bookstore.utils;

import java.util.ResourceBundle;

public class MessageUtil {
    public static final String LIST_ACTION_MSG = "list-action-msg";
    public static final String VIEW_ALL_BOOKS_MSG = "view-all-books-msg";
    public static final String VIEW_ALL_BOOKS_MSG_EMPTY ="view-all-books-empty-msg";
    public static final String ADD_BOOK_MSG ="add-book-msg";
    public static final String BOOK_TITLE_MSG ="book-title-msg";
    public static final String BOOK_AUTHOR_MSG ="book-author-msg";
    public static final String BOOK_DESCRIPTION_MSG ="book-description-msg";
    public static final String SAVE_BOOK_SUCCESS_MSG ="save-book-success-msg";
    public static final String BOOK_ID_MSG ="book-id-msg";
    public static final String EDIT_BOOK_CHOOSE_MSG ="edit-book-choose-msg";
    public static final String EDIT_BOOK_MSG ="edit-book-msg";
    public static final String SEARCH_BOOK_SUCCESS_MSG = "search-book-success-msg";
    public static final String SEARCH_BOOK_MSG = "search-book-msg";
    public static final String BOOKSTORE_TITLE ="bookstore-title";
    public static final String VIEW_ALL_BOOKS_TITLE = "view-all-books-title";
    public static final String ADD_BOOKS_TITLE = "add-books-title";
    public static final String EDIT_BOOKS_TITLE = "edit-books-title";
    public static final String SEARCH_BOOKS_TITLE ="search-book-title";
    static ResourceBundle resourceBundle = ResourceBundle.getBundle("messages");

    public static String getMsg(String key){
        return resourceBundle.getString(key);
    }
}

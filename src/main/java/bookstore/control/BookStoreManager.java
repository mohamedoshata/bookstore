package bookstore.control;

import bookstore.entities.ActionEnum;
import bookstore.entities.Book;
import bookstore.utils.MessageUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BookStoreManager {

    List<ActionEnum> actionEnumList = Arrays.asList(ActionEnum.values());
    public static List<Book> bookList;
    public static int maxId ;

    File file = new File("bookstoreDB.json");

    /**
     * iterates on the ActionEnum to print them to allow user to choose one of them
     */
    public void listActions(){

        System.out.println(MessageUtil.getMsg(MessageUtil.BOOKSTORE_TITLE));

        for(ActionEnum action : actionEnumList){
            System.out.println(action.getKey()+")"+action.getValue());
        }

        String listActionMsg = MessageUtil.getMsg(MessageUtil.LIST_ACTION_MSG);
        listActionMsg = MessageFormat.format(listActionMsg,1,actionEnumList.size());
        System.out.println(listActionMsg);

            Scanner scanner = new Scanner(System.in);
            int selectedAction = scanner.nextInt();
            startAction(selectedAction);

    }

    /**
     *
     * @param selectedAction
     */
    private void startAction(int selectedAction){
        if(selectedAction == ActionEnum.ViewAll.getKey()){
            viewAllBooks();
        }else if (selectedAction == ActionEnum.AddNew.getKey()){
            addNewBook();
        }else if (selectedAction == ActionEnum.Edit.getKey()){
            editBook();
        }else if (selectedAction == ActionEnum.Search.getKey()){
            searchBook();
        }else if(selectedAction == ActionEnum.Save.getKey()){
            exit();
        }
    }

    /**
     *  use the ObjectMapper to write the bookList in the DB file
     */
    private void exit() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(file,bookList);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * allow user to enter the keywords for the books which looking for
     * then call queryOnBook() to return the matched list
     * then allow the user to choose on off the matched books to view its details
     */
    private void searchBook() {
        System.out.println(MessageUtil.getMsg(MessageUtil.SEARCH_BOOKS_TITLE));
        System.out.println(MessageUtil.getMsg(MessageUtil.SEARCH_BOOK_MSG));
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        if(input.trim().length() == 0){
            listActions();
            return;
        }
        List<Book> matchedBooks =  queryOnBook(input);
        if(matchedBooks == null || matchedBooks.isEmpty()){
            System.out.println("no books matched to your input");
            searchBook();
            return;
        }

        System.out.println(MessageUtil.getMsg(MessageUtil.SEARCH_BOOK_SUCCESS_MSG));
        scanner = new Scanner(System.in);
        printBookList(matchedBooks);
        input = scanner.nextLine();
        if(input.trim().length() == 0){
            searchBook();
            return;
        }
        int bookId = Integer.parseInt(input);
        printBookDetails(bookId);
        searchBook();
    }

    /**
     * loop on bookList and compare the input with the title, author and description
     * if one of them contains the input keyword the book added to the matched list
     * then return the matched list
     * @param input
     * @return List<Book>
     */
    private List<Book> queryOnBook(String input) {
        List<Book> matchedBooks = new ArrayList<Book>();
        for(Book book :bookList){
            if(book.getTitle().contains(input) || book.getAuother().contains(input)|| book.getDescription().contains(input)){
                matchedBooks.add(book);
            }
        }
        return matchedBooks;
    }

    /**
     * print all bookList then let the user select which book to edit
     * */
    private void editBook() {
        System.out.println(MessageUtil.getMsg(MessageUtil.EDIT_BOOKS_TITLE));
        printBookList(bookList);
        System.out.println(MessageUtil.getMsg(MessageUtil.EDIT_BOOK_CHOOSE_MSG));
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        if(input.trim().length() == 0){
            listActions();
            return;
        }

        int bookId = Integer.parseInt(input);
        Book book = findBookById(bookId);
        if(book != null) {
            System.out.println(MessageUtil.getMsg(MessageUtil.EDIT_BOOK_MSG));
            editBookTitle(book);
            editBookAuthor(book);
            editBookDescription(book);
            String successMsg = MessageFormat.format(MessageUtil.getMsg(MessageUtil.SAVE_BOOK_SUCCESS_MSG),book.getId());
            System.out.println(successMsg);
        }
        editBook();
    }

    private void editBookDescription(Book book) {
        System.out.println(MessageUtil.getMsg(MessageUtil.BOOK_DESCRIPTION_MSG)+"["+book.getDescription()+"]:");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        if(input.trim().length() == 0){
            return;
        }
        book.setDescription(input);
    }

    private void editBookAuthor(Book book) {
        System.out.println(MessageUtil.getMsg(MessageUtil.BOOK_AUTHOR_MSG)+"["+book.getAuother()+"]:");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        if(input.trim().length() == 0){
            return;
        }
        book.setAuother(input);
    }

    private void editBookTitle(Book book) {
        System.out.println(MessageUtil.getMsg(MessageUtil.BOOK_TITLE_MSG)+"["+book.getTitle()+"]:");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        if(input.trim().length() == 0){
            return;
        }
        book.setTitle(input);
    }

    /**
     *  initialize new Book and generate id and increment maxId then add the new Book
     *  to the bookList
     */
    private void addNewBook() {
        System.out.println(MessageUtil.getMsg(MessageUtil.ADD_BOOKS_TITLE));
        Book newBook = new Book();
        Scanner scanner = new Scanner(System.in);
        System.out.println(MessageUtil.getMsg(MessageUtil.ADD_BOOK_MSG));
        String input;
        do {
            System.out.println(MessageUtil.getMsg(MessageUtil.BOOK_TITLE_MSG));
            input = scanner.nextLine();
            newBook.setTitle(input);
        }while(newBook.getTitle().trim().length() == 0);
        do {
            System.out.println(MessageUtil.getMsg(MessageUtil.BOOK_AUTHOR_MSG));
            input = scanner.nextLine();
            newBook.setAuother(input);
        }while(newBook.getAuother().trim().length() == 0);
        do {
            System.out.println(MessageUtil.getMsg(MessageUtil.BOOK_DESCRIPTION_MSG));
            input = scanner.nextLine();
            newBook.setDescription(input);
        }while(newBook.getDescription().trim().length() == 0);

        newBook.setId(++maxId);
        bookList.add(newBook);
        String successMsg = MessageFormat.format(MessageUtil.getMsg(MessageUtil.SAVE_BOOK_SUCCESS_MSG),newBook.getId());
        System.out.println(successMsg);

        listActions();
    }

    /**
     *  check if bookList is not empty print it and check if user need to show book details
     */
    private void viewAllBooks ()
    {
        System.out.println(MessageUtil.getMsg(MessageUtil.VIEW_ALL_BOOKS_TITLE));
        String input;
        if(bookList == null || bookList.isEmpty()){
            System.out.println(MessageUtil.getMsg(MessageUtil.VIEW_ALL_BOOKS_MSG_EMPTY));
            Scanner scanner = new Scanner(System.in);
            input = scanner.nextLine();

            if(input.trim().length() == 0){
                listActions();
            }else{
                viewAllBooks();
            }
        }else{
            printBookList(bookList);
            System.out.println(MessageUtil.getMsg(MessageUtil.VIEW_ALL_BOOKS_MSG));
            Scanner scanner = new Scanner(System.in);
            input = scanner.nextLine();

            if(input.trim().length() == 0){
                listActions();
                return;
            }
            int bookId = Integer.parseInt(input);
            printBookDetails(bookId);
            viewAllBooks();
        }

    }

    /**
     * loop on the parameter list and print it as "[id] BookTitle"
     * @param bookList
     */
    private void printBookList(List<Book> bookList) {
        for(Book book :bookList){
            System.out.println("["+book.getId()+"] "+book.getTitle());
        }
    }

    /**
     * use method findById() to get Book object
     * then print it if it is not null
     * @param bookId
     */
    private void printBookDetails(int bookId){
        Book book = findBookById(bookId);
        if(book != null) {
            System.out.println(MessageUtil.getMsg(MessageUtil.BOOK_ID_MSG) + book.getId());
            System.out.println(MessageUtil.getMsg(MessageUtil.BOOK_TITLE_MSG) + book.getTitle());
            System.out.println(MessageUtil.getMsg(MessageUtil.BOOK_AUTHOR_MSG) + book.getAuother());
            System.out.println(MessageUtil.getMsg(MessageUtil.BOOK_DESCRIPTION_MSG) + book.getDescription());
        }
    }

    /**
     * loadBooks From file
     */
    public void loadBooks() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try
        {
            bookList =  mapper.readValue(file, new TypeReference<List<Book>>(){});
            setMaxBookId();
        }
        catch (MismatchedInputException e){
            bookList = new ArrayList<Book>();
        }
        catch (FileNotFoundException e){
            System.err.println("can't start app without bookstoreDB.json in home directory");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * called once the application starts to increment the generated ids
     */
    private void setMaxBookId() {
        maxId = 0;
        for(Book book:bookList){
            if(book.getId() > maxId){
                maxId = book.getId();
            }
        }
    }

    /**
     * loop on bookList and compare by id
     * @param id
     * @return Book
     */
    private Book findBookById(int id){
        for(Book book :bookList) {
            if (book.getId() == id) {
                return book;
            }
        }
        return null;
    }
}
